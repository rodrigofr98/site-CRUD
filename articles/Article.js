const Sequelize = require("sequelize")
const connection = require("../database/database")
const Category = require("../categories/Category") 

const Article = connection.define('articles', {
    title: { 
        type: Sequelize.STRING,
        allowNull: false
    },slug: { 
        type: Sequelize.STRING,
        allowNull: false
    },
    body: { 
        type: Sequelize.TEXT,
        allowNull: false
    }
})

Category.hasMany(Article) 
Article.belongsTo(Category) 
//Article.sync({force: true}) metodo sync com force: true vai recriar a tabela sempre que vc criar o programa. Essa linha de sincornizacao deve ser removida caso contrario ele vai tentar criar a tabela toda vez q o projeto for rodado

module.exports = Article