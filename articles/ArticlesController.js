
const express = require("express")
const router = express.Router()
const Category = require("../categories/Category")
const Article = require("./Article")
const slugify = require("slugify")
const adminAuth = require("../middlewares/adminAuth")


router.get("/admin/articles", adminAuth, (req, res) => {
    Article.findAll({
        include: [{
            model: Category
        }]
    }).then(articles => {
        var log = req.session.user
        res.render("admin/articles/index", {
            articles,
            log,
        })
    })
})

router.get("/admin/articles/new", adminAuth, (req, res) => {
    Category.findAll().then(categories => {
        var log = req.session.user
        res.render("admin/articles/new", {
            categories,
            log,
        })
    })
})

router.post("/articles/save", (req, res) => {
    var title = req.body.title
    var body = req.body.body
    var category = req.body.category

    Article.create({
        title,
        slug: slugify(title),
        body,
        categoryId: category
    }).then(() => {
        res.redirect("/admin/articles")
    })
})

router.post("/articles/delete", (req, res) => {
    var id = req.body.id
    if (id != undefined) {

        if (!isNaN(id)) { 

            Article.destroy({ 
                where: {
                    id,
                }
            }).then(() => {
                res.redirect("/admin/articles")
            })

        } else { 
            res.redirect("/admin/articles")
        }

    } else { 
        res.redirect("/admin/articles")
    }
})


router.get("/admin/articles/edit/:id", adminAuth, (req, res) => {
    var id = req.params.id
    if (isNaN(id)) {
        res.redirect("/admin/articles")
    }
    Article.findByPk(id).then(article => {
        if (article != undefined) {
            Category.findAll().then(categories => {
                var log = req.session.user
                res.render("admin/articles/edit", {
                    article,
                    categories,
                    log,
                })
            })
        } else {
            res.redirect("/admin/articles")
        }
    }).catch(err => {
        res.redirect("/admin/articles")
    })
})

router.post("/articles/update", (req, res) => {
    var id = req.body.id
    var title = req.body.title
    var categoryId = req.body.categoryId
    var body = req.body.body
    Article.update({
        title,
        slug: slugify(title),
        body,
        categoryId,
    }, {
        where: {
            id,
        }
    }).then(() => {
        res.redirect("/admin/articles")
    })
})

router.get("/articles/page/:num", (req, res) => {
    var num = req.params.num
    var offset = 0

    if (isNaN(num) || (num < 1)) {
        res.redirect("/articles/page/1")
    }

    if (num == 1) {
        offset = 0      
    } else {
        offset = parseInt(num-1) * 4 
    }
    
    Article.findAndCountAll({
        limit: 4,
        offset,
    }).then(articles => {
        var next 
        var pages = Math.ceil(articles.count/4)

        if (offset + 4 >= articles.count) {
            next = false
        } else {
            next = true
        }

        if (!next && (num > pages)) {
            res.redirect("/articles/page/1")
        }

        var content = {
            next,
            articles,
            num: parseInt(num),
        } 
        
        Category.findAll().then(categories => {
            var log = req.session.user
            res.render("page", {
                content,
                categories,
                log,
            })
        })       
    })
})


module.exports = router