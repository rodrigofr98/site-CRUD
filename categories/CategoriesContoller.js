const express = require("express")
const router = express.Router()
const Category = require("./Category")
const slugify = require("slugify")
const adminAuth = require("../middlewares/adminAuth")

router.post("/categories/save", (req, res) => { 
    var title = req.body.title 
    if (title != undefined) {
        //Sequelize...
        Category.create({
            title,
            slug: slugify(title)
        }).then(() => {
            res.redirect("/admin/categories")
        })

    } else {
        res.redirect("/admin/categories/new")
    }
})

router.get("/admin/categories/new", adminAuth, (req, res) => {
    var log = req.session.user
    res.render("admin/categories/new", {log,})
})

router.get("/admin/categories", adminAuth, (req, res) => {
    Category.findAll().then(categories => {
        var log = req.session.user
        res.render("admin/categories/index", {
            categories,
            log,
        }) 
    })
})

router.post("/categories/delete", (req, res) => {
    var id = req.body.id
    if (id != undefined) {

        if (!isNaN(id)) { 

            Category.destroy({ 
                where: {
                    id,
                }
            }).then(() => {
                res.redirect("/admin/categories")
            })

        } else { 
            res.redirect("/admin/categories")
        }

    } else { 
        res.redirect("/admin/categories")
    }
})

router.get("/admin/categories/edit/:id", adminAuth, (req, res) => {
    var id = req.params.id
    if (isNaN(id)) {
        res.redirect("/admin/categories")
    }
    Category.findByPk(id).then(category => {
        if (category != undefined) {
            var log = req.session.user
            res.render("admin/categories/edit", {
                category,
                log,
            }) 
        } else {
            res.redirect("/admin/categories")
        }
    }).catch(erro => {
        res.redirect("/admin/categories")
    })
})

router.post("/categories/update", (req, res) => {
    var id = req.body.id
    var title = req.body.title
    Category.update({
        title,
        slug: slugify(title)
    }, {
        where: {
            id,
        }
    }).then(() => {
        res.redirect("/admin/categories")
    })
})

module.exports = router