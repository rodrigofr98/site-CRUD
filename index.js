const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const session = require("express-session")
const connection = require('./database/database')
const categoriesController = require("./categories/CategoriesContoller")
const articlesController = require("./articles/ArticlesController")
const usersController = require("./users/UsersController")
const Article = require("./articles/Article")
const Category = require("./categories/Category")
const User = require("./users/User")

//view engine
app.set('view engine', 'ejs')


//sessions
app.use(session({
    secret: "qualquercoisa",
    cookie: {maxAge: 300000}
}))


//configuracoes da pasta arquivos estaticos
app.use(express.static('public'))

//body parser
app.use(bodyParser.urlencoded({
    extended: false
})) 
app.use(bodyParser.json()) 

//database
connection 
    .authenticate()
    .then(() => { //caso seja realizada
        console.log('conexao com o banco de dados realizada')
    }).catch((error) => { //caso nao seja realizada
        console.log(error)
    })


app.use("/", categoriesController)
app.use("/", articlesController)
app.use("/", usersController)

//rotas "basicas" da aplicacao

app.get("/category/:slug", (req, res) => {
    var slug = req.params.slug
    Category.findOne({
        where: {
            slug,
        },
        include: [{
            model: Article
        }]
    }).then(category => {
        if (category != undefined) {
            Category.findAll().then(categories => {
                var log = req.session.user
                res.render("category", {
                    articles: category.articles,
                    categories,
                    log,
                })
            })
        } else {
            res.redirect("/")
        }
    }).catch(err => {
        res.redirect("/")
    })
})


app.get('/', (req, res) => {
    Article.findAll({
        order: [
            ["id", "DESC"]
        ]
    }).then(articles => {
        Category.findAll().then(categories => {
            var log = req.session.user
            res.render('index', {
                articles,
                categories,
                log,
            })
        })
    })
})

app.get('/:slug', (req, res) => {
    var slug = req.params.slug
    Article.findOne({
        where: {
            slug,
        }
    }).then(article => {
        if (article != undefined) {
            Category.findAll().then(categories => {
                var log = req.session.user
                res.render('article', {
                    article,
                    categories,
                    log,
                })
            })
        } else {
            res.redirect("/")
        }
    }).catch(err => {
        res.redirect("/")
    })
})

app.listen('8080', () => { 
    console.log('O servidor esta rodando!')
})