const express = require("express")
const router = express.Router()
const User = require("./User")
const bcrypt = require("bcryptjs")
const slugify = require("slugify")
const adminAuth = require("../middlewares/adminAuth")


router.get("/admin/users", adminAuth, (req, res) => {
    res.send("listagem de usuarios")
})

router.get("/admin/users/create", (req, res) => {
    var log = req.session.user
    res.render("admin/users/create", {log,})
})

router.post("/users/create", (req, res) => {
    var email = req.body.email
    var password = req.body.password

    User.findOne({
        where: {
            email,
        }
    }).then(user => {
        if(user == undefined) {
            var salt = bcrypt.genSaltSync(10)
            var hash = bcrypt.hashSync(password, salt)
        
            User.create({
                email,
                password: hash
            }).then(() => {
                res.redirect("/")
            }).catch(err => {
                res.redirect("/")
            })
        } else {
            res.redirect("/admin/users/create")
        }
    })
})


router.get("/login", (req, res) => {
    var log = req.session.user
    res.render("admin/users/login", {log,})
})

router.post("/authenticate", (req, res) => {

    var email = req.body.email
    var password = req.body.password

    User.findOne({
        where: {
            email,
        }
    }).then(user => {
        if (user != undefined) {

            var correct = bcrypt.compareSync(password,user.password)

            if(correct){
                req.session.user = {
                    id: user.id,
                    email: user.email
                }
                res.redirect("/")
            } else {
                res.redirect("/login")
            }
        } else {
            res.redirect("/login")
        }
    })

})

router.get("/logout", (req,res) => {
    req.session.user = undefined
    res.redirect("/")
})


module.exports = router